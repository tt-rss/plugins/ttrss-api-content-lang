<?php
class Api_Content_Lang extends Plugin {
	private $host;

	function about() {
		return array(1.0,
			"Sets lang=zh to article content returned via API (Chrome issue workaround)",
			"fox");
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_RENDER_ARTICLE_API, $this);
	}

	function hook_render_article_api($row) {
		$article = isset($row['headline']) ? $row['headline'] : $row['article'];

		$article['content'] = "<div lang='zh'>" . $article['content'] . "</div>";

		return $article;
	}

	function api_version() {
		return 2;
	}
}
